## Code Challenge - Digital Republic

Este projeto foi elaborado seguindo os parametros estabelecidos pelo Code Challenge da Digital Republic, utilizando bibliotecas e plugins para melhor trabalhar o dinamismo do site.

### Bibliotecas e Plugins Utilizados:

- **Bootstrap**
- **Jquery**
- **Sweetalert2**

## Sobre o Teste

O teste constitui de aprensentar ao usuário o calculo de litros de tinta utilizados para pintar uma área de quatro paredes,considerando desconto de portas e janelas na operação final. Toda a lógica referente a calculo foi colocada no arquivo **calculo.js.**

## Execução do Projeto

Para executar oprojeto é necessário baixar todos arquivos e pastas, e abrir ele em qualquer navegador.
