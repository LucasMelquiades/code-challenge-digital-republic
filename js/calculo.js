//Calculo Parede 1
function metroquad1() {
var alt1 = parseFloat(document.getElementById('alt-parede-1').value);
 var larg1 = parseFloat(document.getElementById('larg-parede-1').value);
 var porta = 0.8 * 1.9;
 var janela = 2 * 1.2;
 var areapj = porta + janela;
 var resul1 = larg1 * alt1;
 console.log(areapj);


 if ((document.getElementById('porta-1').checked) && (document.getElementById('janela-1').checked)) {
     //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
     if ((resul1/2) < areapj) {
         Swal.fire({
             icon: 'error',
             text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("porta-1").checked = false;
         document.getElementById("janela-1").checked = false;
     } else if (((resul1-areapj) < 1)||((resul1-areapj) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-1").checked = false;
         document.getElementById("janela-1").checked = false;
         document.getElementById("alt-parede-1").value = '';
         document.getElementById("larg-parede-1").value = '';
         document.getElementById("total-1").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul1 - areapj)+'.'
         })
         document.getElementById("total-1").value = resul1 - areapj;
     }
 } else if (document.getElementById('porta-1').checked) {
     //Verifica se a altura da parede é suficiente para a porta
     if (alt1 < 2.20) {
         Swal.fire({
             icon: 'error',
             text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
         })
         document.getElementById("porta-1").checked = false;
     }
     //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
     else if ((resul1/2) < porta) {
         Swal.fire({
             icon: 'error',
             text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("porta-1").checked = false;
     } else if (((resul1-porta) < 1)||((resul1-porta) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-1").checked = false;
         document.getElementById("janela-1").checked = false;
         document.getElementById("alt-parede-1").value = '';
         document.getElementById("larg-parede-1").value = '';
         document.getElementById("total-1").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul1 - porta)+'.'
         })
         document.getElementById("total-1").value = resul1 - porta;
     }
 } else if (document.getElementById('janela-1').checked) {
     //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
     if ((resul1/2) < janela) {
         Swal.fire({
             icon: 'error',
             text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("janela-1").checked = false;
     } else if (((resul1-janela) < 1)||((resul1-janela) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-1").checked = false;
         document.getElementById("janela-1").checked = false;
         document.getElementById("alt-parede-1").value = '';
         document.getElementById("larg-parede-1").value = '';
         document.getElementById("total-1").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul1 - janela)+'.'
         })
         document.getElementById("total-1").value = resul1 - janela;
     }
 } else if ((resul1 < 1)||(resul1 > 15)){
     Swal.fire({
         icon: 'error',
         text: 'A Área Quadrada da Parede deve ficar entre 1 e 15!'
     })
     document.getElementById("alt-parede-1").value = '';
     document.getElementById("larg-parede-1").value = '';
     document.getElementById("total-1").value = '';
 } else {
     document.getElementById("total-1").value = resul1;
 }
}

function check1(){
    var alt1 = document.getElementById('alt-parede-1').value;
    var larg1 = document.getElementById('larg-parede-1').value;
    var porta = 0.8 * 1.9;
    var janela = 2 * 1.2;
    var areapj = porta + janela;
    var resul1 = larg1 * alt1;
    if ((alt1 === '')||(larg1 === '')){
        Swal.fire({
            icon: 'error',
            text: 'Preencha primeira as medidas para inserir uma Porta ou Janela'
        })
        document.getElementById("porta-1").checked = false;
        document.getElementById("janela-1").checked = false;
    } else if ((document.getElementById('porta-1').checked) && (document.getElementById('janela-1').checked)) {
        //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
        if ((resul1/2) < areapj) {
            Swal.fire({
                icon: 'error',
                text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
            })
            document.getElementById("porta-1").checked = false;
            document.getElementById("janela-1").checked = false;
            document.getElementById("alt-parede-1").value = '';
            document.getElementById("larg-parede-1").value = '';
            document.getElementById("total-1").value = '';
        } else if (((resul1-areapj) < 1)||((resul1-areapj) > 15)){
            Swal.fire({
                icon: 'error',
                text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
            })
            document.getElementById("porta-1").checked = false;
            document.getElementById("janela-1").checked = false;
            document.getElementById("alt-parede-1").value = '';
            document.getElementById("larg-parede-1").value = '';
            document.getElementById("total-1").value = '';
        } else {
            Swal.fire({
                icon: 'success',
                text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul1 - areapj)+'.'
            })
            document.getElementById("total-1").value = resul1 - areapj;
        }
    } else if (document.getElementById('porta-1').checked) {
        //Verifica se a altura da parede é suficiente para a porta
        if (alt1 < 2.20) {
            Swal.fire({
                icon: 'error',
                text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
            })
            document.getElementById("porta-1").checked = false;
        }
        //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
        else if ((resul1/2) < porta) {
            Swal.fire({
                icon: 'error',
                text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
            })
            document.getElementById("porta-1").checked = false;
        } else if (((resul1-porta) < 1)||((resul1-porta) > 15)){
            Swal.fire({
                icon: 'error',
                text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
            })
            document.getElementById("porta-1").checked = false;
            document.getElementById("janela-1").checked = false;
            document.getElementById("alt-parede-1").value = '';
            document.getElementById("larg-parede-1").value = '';
            document.getElementById("total-1").value = '';
        } else {
            Swal.fire({
                icon: 'success',
                text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul1 - porta)+'.'
            })
            document.getElementById("total-1").value = resul1 - porta;
        }
    } else if (document.getElementById('janela-1').checked) {
        //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
        if ((resul1/2) < janela) {
            Swal.fire({
                icon: 'error',
                text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
            })
            document.getElementById("janela-1").checked = false;
        } else if (((resul1-janela) < 1)||((resul1-janela) > 15)){
            Swal.fire({
                icon: 'error',
                text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
            })
            document.getElementById("porta-1").checked = false;
            document.getElementById("janela-1").checked = false;
            document.getElementById("alt-parede-1").value = '';
            document.getElementById("larg-parede-1").value = '';
            document.getElementById("total-1").value = '';
        } else {
            Swal.fire({
                icon: 'success',
                text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul1 - janela)+'.'
            })
            document.getElementById("total-1").value = resul1 - janela;
        }
    } else if (!document.getElementById('janela-1').checked) {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Janela): '+resul1+'.'
        })
        document.getElementById("total-1").value = resul1;
    } else if (!document.getElementById('porta-1').checked) {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Porta): '+resul1+'.'
        })
        document.getElementById("total-1").value = resul1;
    }
   }

//Calculo Parede 2
function metroquad2() {
    var alt2 = parseFloat(document.getElementById('alt-parede-2').value);
     var larg2 = parseFloat(document.getElementById('larg-parede-2').value);
     var porta = 0.8 * 1.9;
     var janela = 2 * 1.2;
     var areapj = porta + janela;
     var resul2 = larg2 * alt2;
     console.log(areapj);
    
    
     if ((document.getElementById('porta-2').checked) && (document.getElementById('janela-2').checked)) {
         //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
         if ((resul2/2) < areapj) {
             Swal.fire({
                 icon: 'error',
                 text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
             })
             document.getElementById("porta-2").checked = false;
             document.getElementById("janela-2").checked = false;
         } else if (((resul2-areapj) < 1)||((resul2-areapj) > 15)){
             Swal.fire({
                 icon: 'error',
                 text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
             })
             document.getElementById("porta-2").checked = false;
             document.getElementById("janela-2").checked = false;
             document.getElementById("alt-parede-2").value = '';
             document.getElementById("larg-parede-2").value = '';
             document.getElementById("total-2").value = '';
         } else {
             Swal.fire({
                 icon: 'success',
                 text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul2 - areapj)+'.'
             })
             document.getElementById("total-2").value = resul2 - areapj;
         }
     } else if (document.getElementById('porta-2').checked) {
         //Verifica se a altura da parede é suficiente para a porta
         if (alt2 < 2.20) {
             Swal.fire({
                 icon: 'error',
                 text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
             })
             document.getElementById("porta-2").checked = false;
         }
         //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
         else if ((resul2/2) < porta) {
             Swal.fire({
                 icon: 'error',
                 text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
             })
             document.getElementById("porta-2").checked = false;
         } else if (((resul2-porta) < 1)||((resul2-porta) > 15)){
             Swal.fire({
                 icon: 'error',
                 text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
             })
             document.getElementById("porta-2").checked = false;
             document.getElementById("janela-2").checked = false;
             document.getElementById("alt-parede-2").value = '';
             document.getElementById("larg-parede-2").value = '';
             document.getElementById("total-2").value = '';
         } else {
             Swal.fire({
                 icon: 'success',
                 text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul2 - porta)+'.'
             })
             document.getElementById("total-2").value = resul2 - porta;
         }
     } else if (document.getElementById('janela-2').checked) {
         //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
         if ((resul2/2) < janela) {
             Swal.fire({
                 icon: 'error',
                 text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
             })
             document.getElementById("janela-2").checked = false;
         } else if (((resul2-janela) < 1)||((resul2-janela) > 15)){
             Swal.fire({
                 icon: 'error',
                 text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
             })
             document.getElementById("porta-2").checked = false;
             document.getElementById("janela-2").checked = false;
             document.getElementById("alt-parede-2").value = '';
             document.getElementById("larg-parede-2").value = '';
             document.getElementById("total-2").value = '';
         } else {
             Swal.fire({
                 icon: 'success',
                 text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul2 - janela)+'.'
             })
             document.getElementById("total-2").value = resul2 - janela;
         }
     } else if ((resul2 < 1)||(resul2 > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede deve ficar entre 1 e 15!'
         })
         document.getElementById("alt-parede-2").value = '';
         document.getElementById("larg-parede-2").value = '';
         document.getElementById("total-2").value = '';
     } else {
         document.getElementById("total-2").value = resul2;
     }
    }

function check2(){
 var alt2 = document.getElementById('alt-parede-2').value;
 var larg2 = document.getElementById('larg-parede-2').value;
 var porta = 0.8 * 1.9;
 var janela = 2 * 1.2;
 var areapj = porta + janela;
 var resul2 = larg2 * alt2;
 if ((alt2 === '')||(larg2 === '')){
     Swal.fire({
         icon: 'error',
         text: 'Preencha primeira as medidas para inserir uma Porta ou Janela'
     })
     document.getElementById("porta-2").checked = false;
     document.getElementById("janela-2").checked = false;
 } else if ((document.getElementById('porta-2').checked) && (document.getElementById('janela-2').checked)) {
     //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
     if ((resul2/2) < areapj) {
         Swal.fire({
             icon: 'error',
             text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("porta-2").checked = false;
         document.getElementById("janela-2").checked = false;
         document.getElementById("alt-parede-2").value = '';
         document.getElementById("larg-parede-2").value = '';
         document.getElementById("total-2").value = '';
     } else if (((resul2-areapj) < 1)||((resul2-areapj) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-2").checked = false;
         document.getElementById("janela-2").checked = false;
         document.getElementById("alt-parede-2").value = '';
         document.getElementById("larg-parede-2").value = '';
         document.getElementById("total-2").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul2 - areapj)+'.'
         })
         document.getElementById("total-2").value = resul2 - areapj;
     }
 } else if (document.getElementById('porta-2').checked) {
     //Verifica se a altura da parede é suficiente para a porta
     if (alt2 < 2.20) {
         Swal.fire({
             icon: 'error',
             text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
         })
         document.getElementById("porta-2").checked = false;
     }
     //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
     else if ((resul2/2) < porta) {
         Swal.fire({
             icon: 'error',
             text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("porta-2").checked = false;
     } else if (((resul2-porta) < 1)||((resul2-porta) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-2").checked = false;
         document.getElementById("janela-2").checked = false;
         document.getElementById("alt-parede-2").value = '';
         document.getElementById("larg-parede-2").value = '';
         document.getElementById("total-2").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul2 - porta)+'.'
         })
         document.getElementById("total-2").value = resul2 - porta;
     }
 } else if (document.getElementById('janela-2').checked) {
     //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
     if ((resul2/2) < janela) {
         Swal.fire({
             icon: 'error',
             text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("janela-2").checked = false;
     } else if (((resul2-janela) < 1)||((resul2-janela) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-2").checked = false;
         document.getElementById("janela-2").checked = false;
         document.getElementById("alt-parede-2").value = '';
         document.getElementById("larg-parede-2").value = '';
         document.getElementById("total-2").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul2 - janela)+'.'
         })
         document.getElementById("total-2").value = resul2 - janela;
     }
 } else if (!document.getElementById('janela-2').checked) {
     Swal.fire({
         icon: 'success',
         text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Janela): '+resul2+'.'
     })
     document.getElementById("total-2").value = resul2;
 } else if (!document.getElementById('porta-2').checked) {
     Swal.fire({
         icon: 'success',
         text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Porta): '+resul2+'.'
     })
     document.getElementById("total-2").value = resul2;
 }
}


//Calculo Parede 3
function metroquad3() {
var alt3 = parseFloat(document.getElementById('alt-parede-3').value);
var larg3 = parseFloat(document.getElementById('larg-parede-3').value);
var porta = 0.8 * 1.9;
var janela = 2 * 1.2;
var areapj = porta + janela;
var resul3 = larg3 * alt3;
console.log(areapj);


if ((document.getElementById('porta-3').checked) && (document.getElementById('janela-3').checked)) {
    //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
    if ((resul3/2) < areapj) {
        Swal.fire({
            icon: 'error',
            text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
    } else if (((resul3-areapj) < 1)||((resul3-areapj) > 15)){
        Swal.fire({
            icon: 'error',
            text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul3 - areapj)+'.'
        })
        document.getElementById("total-3").value = resul3 - areapj;
    }
} else if (document.getElementById('porta-3').checked) {
    //Verifica se a altura da parede é suficiente para a porta
    if (alt3 < 2.20) {
        Swal.fire({
            icon: 'error',
            text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
        })
        document.getElementById("porta-3").checked = false;
    }
    //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
    else if ((resul3/2) < porta) {
        Swal.fire({
            icon: 'error',
            text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
        })
        document.getElementById("porta-3").checked = false;
    } else if (((resul3-porta) < 1)||((resul3-porta) > 15)){
        Swal.fire({
            icon: 'error',
            text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul3 - porta)+'.'
        })
        document.getElementById("total-3").value = resul3 - porta;
    }
} else if (document.getElementById('janela-3').checked) {
    //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
    if ((resul3/2) < janela) {
        Swal.fire({
            icon: 'error',
            text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
        })
        document.getElementById("janela-3").checked = false;
    } else if (((resul3-janela) < 1)||((resul3-janela) > 15)){
        Swal.fire({
            icon: 'error',
            text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul3 - janela)+'.'
        })
        document.getElementById("total-3").value = resul3 - janela;
    }
} else if ((resul3 < 1)||(resul3 > 15)){
    Swal.fire({
        icon: 'error',
        text: 'A Área Quadrada da Parede deve ficar entre 1 e 15!'
    })
    document.getElementById("alt-parede-3").value = '';
    document.getElementById("larg-parede-3").value = '';
    document.getElementById("total-3").value = '';
} else {
    document.getElementById("total-3").value = resul3;
}
}

function check3(){
var alt3 = document.getElementById('alt-parede-3').value;
var larg3 = document.getElementById('larg-parede-3').value;
var porta = 0.8 * 1.9;
var janela = 2 * 1.2;
var areapj = porta + janela;
var resul3 = larg3 * alt3;
if ((alt3 === '')||(larg3 === '')){
    Swal.fire({
        icon: 'error',
        text: 'Preencha primeira as medidas para inserir uma Porta ou Janela'
    })
    document.getElementById("porta-3").checked = false;
    document.getElementById("janela-3").checked = false;
} else if ((document.getElementById('porta-3').checked) && (document.getElementById('janela-3').checked)) {
    //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
    if ((resul3/2) < areapj) {
        Swal.fire({
            icon: 'error',
            text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else if (((resul3-areapj) < 1)||((resul3-areapj) > 15)){
        Swal.fire({
            icon: 'error',
            text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul3 - areapj)+'.'
        })
        document.getElementById("total-3").value = resul3 - areapj;
    }
} else if (document.getElementById('porta-3').checked) {
    //Verifica se a altura da parede é suficiente para a porta
    if (alt3 < 2.20) {
        Swal.fire({
            icon: 'error',
            text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
        })
        document.getElementById("porta-3").checked = false;
    }
    //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
    else if ((resul3/2) < porta) {
        Swal.fire({
            icon: 'error',
            text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
        })
        document.getElementById("porta-3").checked = false;
    } else if (((resul3-porta) < 1)||((resul3-porta) > 15)){
        Swal.fire({
            icon: 'error',
            text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul3 - porta)+'.'
        })
        document.getElementById("total-3").value = resul3 - porta;
    }
} else if (document.getElementById('janela-3').checked) {
    //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
    if ((resul3/2) < janela) {
        Swal.fire({
            icon: 'error',
            text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
        })
        document.getElementById("janela-3").checked = false;
    } else if (((resul3-janela) < 1)||((resul3-janela) > 15)){
        Swal.fire({
            icon: 'error',
            text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
        })
        document.getElementById("porta-3").checked = false;
        document.getElementById("janela-3").checked = false;
        document.getElementById("alt-parede-3").value = '';
        document.getElementById("larg-parede-3").value = '';
        document.getElementById("total-3").value = '';
    } else {
        Swal.fire({
            icon: 'success',
            text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul3 - janela)+'.'
        })
        document.getElementById("total-3").value = resul3 - janela;
    }
} else if (!document.getElementById('janela-3').checked) {
    Swal.fire({
        icon: 'success',
        text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Janela): '+resul3+'.'
    })
    document.getElementById("total-3").value = resul3;
} else if (!document.getElementById('porta-3').checked) {
    Swal.fire({
        icon: 'success',
        text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Porta): '+resul3+'.'
    })
    document.getElementById("total-3").value = resul3;
}
}


//Calculo Parede 4
function metroquad4() {
    var alt4 = parseFloat(document.getElementById('alt-parede-4').value);
     var larg4 = parseFloat(document.getElementById('larg-parede-4').value);
     var porta = 0.8 * 1.9;
     var janela = 2 * 1.2;
     var areapj = porta + janela;
     var resul4 = larg4 * alt4;
     console.log(areapj);
    
    
     if ((document.getElementById('porta-4').checked) && (document.getElementById('janela-4').checked)) {
         //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
         if ((resul4/2) < areapj) {
             Swal.fire({
                 icon: 'error',
                 text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
             })
             document.getElementById("porta-4").checked = false;
             document.getElementById("janela-4").checked = false;
         } else if (((resul4-areapj) < 1)||((resul4-areapj) > 15)){
             Swal.fire({
                 icon: 'error',
                 text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
             })
             document.getElementById("porta-4").checked = false;
             document.getElementById("janela-4").checked = false;
             document.getElementById("alt-parede-4").value = '';
             document.getElementById("larg-parede-4").value = '';
             document.getElementById("total-4").value = '';
         } else {
             Swal.fire({
                 icon: 'success',
                 text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul4 - areapj)+'.'
             })
             document.getElementById("total-4").value = resul4 - areapj;
         }
     } else if (document.getElementById('porta-4').checked) {
         //Verifica se a altura da parede é suficiente para a porta
         if (alt4 < 2.20) {
             Swal.fire({
                 icon: 'error',
                 text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
             })
             document.getElementById("porta-4").checked = false;
         }
         //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
         else if ((resul4/2) < porta) {
             Swal.fire({
                 icon: 'error',
                 text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
             })
             document.getElementById("porta-4").checked = false;
         } else if (((resul4-porta) < 1)||((resul4-porta) > 15)){
             Swal.fire({
                 icon: 'error',
                 text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
             })
             document.getElementById("porta-4").checked = false;
             document.getElementById("janela-4").checked = false;
             document.getElementById("alt-parede-4").value = '';
             document.getElementById("larg-parede-4").value = '';
             document.getElementById("total-4").value = '';
         } else {
             Swal.fire({
                 icon: 'success',
                 text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul4 - porta)+'.'
             })
             document.getElementById("total-4").value = resul4 - porta;
         }
     } else if (document.getElementById('janela-4').checked) {
         //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
         if ((resul4/2) < janela) {
             Swal.fire({
                 icon: 'error',
                 text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
             })
             document.getElementById("janela-4").checked = false;
         } else if (((resul4-janela) < 1)||((resul4-janela) > 15)){
             Swal.fire({
                 icon: 'error',
                 text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
             })
             document.getElementById("porta-4").checked = false;
             document.getElementById("janela-4").checked = false;
             document.getElementById("alt-parede-4").value = '';
             document.getElementById("larg-parede-4").value = '';
             document.getElementById("total-4").value = '';
         } else {
             Swal.fire({
                 icon: 'success',
                 text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul4 - janela)+'.'
             })
             document.getElementById("total-4").value = resul4 - janela;
         }
     } else if ((resul4 < 1)||(resul4 > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede deve ficar entre 1 e 15!'
         })
         document.getElementById("alt-parede-4").value = '';
         document.getElementById("larg-parede-4").value = '';
         document.getElementById("total-4").value = '';
     } else {
         document.getElementById("total-4").value = resul4;
     }
    }

function check4(){
 var alt4 = document.getElementById('alt-parede-4').value;
 var larg4 = document.getElementById('larg-parede-4').value;
 var porta = 0.8 * 1.9;
 var janela = 2 * 1.2;
 var areapj = porta + janela;
 var resul4 = larg4 * alt4;
 if ((alt4 === '')||(larg4 === '')){
     Swal.fire({
         icon: 'error',
         text: 'Preencha primeira as medidas para inserir uma Porta ou Janela'
     })
     document.getElementById("porta-4").checked = false;
     document.getElementById("janela-4").checked = false;
 } else if ((document.getElementById('porta-4').checked) && (document.getElementById('janela-4').checked)) {
     //Verifica se a orta e janela estão marcadas, caso esteja verifica se a soma das é no minimo 50% da Área Quadrada da Parede
     if ((resul4/2) < areapj) {
         Swal.fire({
             icon: 'error',
             text: 'A soma da janela e da porta deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("porta-4").checked = false;
         document.getElementById("janela-4").checked = false;
         document.getElementById("alt-parede-4").value = '';
         document.getElementById("larg-parede-4").value = '';
         document.getElementById("total-4").value = '';
     } else if (((resul4-areapj) < 1)||((resul4-areapj) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a soma da área da porta e janela deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-4").checked = false;
         document.getElementById("janela-4").checked = false;
         document.getElementById("alt-parede-4").value = '';
         document.getElementById("larg-parede-4").value = '';
         document.getElementById("total-4").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira Parede (com descontos de soma de área de Porta e Janela): '+(resul4 - areapj)+'.'
         })
         document.getElementById("total-4").value = resul4 - areapj;
     }
 } else if (document.getElementById('porta-4').checked) {
     //Verifica se a altura da parede é suficiente para a porta
     if (alt4 < 2.20) {
         Swal.fire({
             icon: 'error',
             text: 'Quando a porta é selecionada é preciso que a altura tenha mais de 2,20 metros!'
         })
         document.getElementById("porta-4").checked = false;
     }
     //Verifica se a porta é no minimo 50% da Área Quadrada da Parede
     else if ((resul4/2) < porta) {
         Swal.fire({
             icon: 'error',
             text: 'A porta deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("porta-4").checked = false;
     } else if (((resul4-porta) < 1)||((resul4-porta) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a área da porta deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-4").checked = false;
         document.getElementById("janela-4").checked = false;
         document.getElementById("alt-parede-4").value = '';
         document.getElementById("larg-parede-4").value = '';
         document.getElementById("total-4").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira parede: (com desconto de área de Porta): '+(resul4 - porta)+'.'
         })
         document.getElementById("total-4").value = resul4 - porta;
     }
 } else if (document.getElementById('janela-4').checked) {
     //Verifica se a janela é no minimo 50% da Área Quadrada da Parede
     if ((resul4/2) < janela) {
         Swal.fire({
             icon: 'error',
             text: 'A janela deve ser no minimo 50% da Área Quadrada da Parede'
         })
         document.getElementById("janela-4").checked = false;
     } else if (((resul4-janela) < 1)||((resul4-janela) > 15)){
         Swal.fire({
             icon: 'error',
             text: 'A Área Quadrada da Parede, menos a área da janela deve ficar entre 1 e 15!'
         })
         document.getElementById("porta-4").checked = false;
         document.getElementById("janela-4").checked = false;
         document.getElementById("alt-parede-4").value = '';
         document.getElementById("larg-parede-4").value = '';
         document.getElementById("total-4").value = '';
     } else {
         Swal.fire({
             icon: 'success',
             text: 'Área Quadrada da Primeira parede: (com desconto de área de Janela): '+(resul4 - janela)+'.'
         })
         document.getElementById("total-4").value = resul4 - janela;
     }
 } else if (!document.getElementById('janela-4').checked) {
     Swal.fire({
         icon: 'success',
         text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Janela): '+resul4+'.'
     })
     document.getElementById("total-4").value = resul4;
 } else if (!document.getElementById('porta-4').checked) {
     Swal.fire({
         icon: 'success',
         text: 'Área Quadrada da Primeira parede: (retirando o desconto de área da Porta): '+resul4+'.'
     })
     document.getElementById("total-4").value = resul4;
 }
}



function calcula() {
 var r1 = parseFloat(document.getElementById("total-1").value);
 var r2 = parseFloat(document.getElementById("total-2").value);
 var r3 = parseFloat(document.getElementById("total-3").value);
 var r4 = parseFloat(document.getElementById("total-4").value);
 var total = r1 + r2 + r3 + r4;
 var totaldiv = total / 5;
 console.log(total, totaldiv);

 if (totaldiv <= 5) {
     document.getElementById("resultado").value = 'Para o total de '+total+' metros quadrados, serão necessários duas latas de 0,5 litros de tinta.';
 } else if ((totaldiv < 6)||(totaldiv <=7)) {
     document.getElementById("resultado").value = 'Para o total de '+total+' metros quadrados, serão necessários três latas de 0,5 litros de tinta.';
 } else if ((totaldiv < 8)||(totaldiv <=10)) {
     document.getElementById("resultado").value = 'Para o total de '+total+' metros quadrados, serão necessários quatro latas de 0,5 litros de tinta.';
 } else if ((totaldiv < 11)||(totaldiv <=12)) {
     document.getElementById("resultado").value = 'Para o total de '+total+' metros quadrados, será necessário uma lata de 2,5 litros de tinta.';
 }
}
